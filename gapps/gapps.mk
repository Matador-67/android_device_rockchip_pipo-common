PRODUCT_COPY_FILES += \
	$(call find-copy-subdir-files,*,device/rockchip/pipo-common/gapps/system/addon.d,system/addon.d) \
	$(call find-copy-subdir-files,*,device/rockchip/pipo-common/gapps/system/app,system/app) \
	$(call find-copy-subdir-files,*,device/rockchip/pipo-common/gapps/system/etc,system/etc) \
	$(call find-copy-subdir-files,*,device/rockchip/pipo-common/gapps/system/etc/permissions,system/etc/permissions) \
	$(call find-copy-subdir-files,*,device/rockchip/pipo-common/gapps/system/etc/preferred-apps,system/etc/preferred-apps) \
	$(call find-copy-subdir-files,*,device/rockchip/pipo-common/gapps/system/framework,system/framework) \
	$(call find-copy-subdir-files,*,device/rockchip/pipo-common/gapps/system/lib,system/lib) \
	$(call find-copy-subdir-files,*,device/rockchip/pipo-common/gapps/system/priv-app,system/priv-app) \
	$(call find-copy-subdir-files,*,device/rockchip/pipo-common/gapps/system/usr/srec/en-US,system/usr/srec/en-US)
